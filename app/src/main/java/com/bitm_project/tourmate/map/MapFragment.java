package com.bitm_project.tourmate.map;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bitm_project.tourmate.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MapFragment extends Fragment implements OnMapReadyCallback {


    private GoogleMap mMap;
    private int PROXIMITY_RADIUS=1000;
    private double latitude;
    private double longitude;
    private String locationSearch;
    public static final int REQUEST_LOCATION_CODE=99;

    String[] permissions={Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};


    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        locationSearch= getArguments().getString("search");


        //getUserPermission();

        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_map, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {

        SupportMapFragment supportMapFragment= (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            //checkLocationPermission();
            getUserPermission();

        }
    }

    private void getUserPermission(){

        if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            requestPermissions(permissions,0);
        }
    }

    private boolean checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)  != PackageManager.PERMISSION_GRANTED )
        {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION))
            {
                ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.ACCESS_FINE_LOCATION },REQUEST_LOCATION_CODE);
            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.ACCESS_FINE_LOCATION },REQUEST_LOCATION_CODE);
            }
            return false;

        }

            return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap= googleMap;

        LatLng bitmLatLang= new LatLng(23.750828, 90.393361);
        googleMap.addMarker(new MarkerOptions().position(bitmLatLang));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bitmLatLang,15));



        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){

            return;

        }
        mMap.setMyLocationEnabled(true);
        getCurrentLocation();
    }


    private void getCurrentLocation() {


        FusedLocationProviderClient fusedLocationProviderClient = new FusedLocationProviderClient(getContext());

        Task location = fusedLocationProviderClient.getLastLocation();

        location.addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {

                Location currentLocation = (Location) task.getResult();
                latitude=currentLocation.getLatitude();
                longitude=currentLocation.getLongitude();
                mMap.moveCamera(CameraUpdateFactory
                        .newLatLngZoom(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())
                                ,15));
                mMap.addMarker(new MarkerOptions().
                        position(new LatLng(currentLocation.getLatitude()
                                ,currentLocation.getLongitude())));
                search();
            }
        });


    }

    public void search(){

        mMap.clear();

        Object dataTransfer[] = new Object[2];

        GetNearbyPlacesData getNearbyPlacesData=new GetNearbyPlacesData();

        String url= getUrl(latitude,longitude,locationSearch);
        dataTransfer[0]=mMap;
        dataTransfer[1]=url;

        getNearbyPlacesData.execute(dataTransfer);
        Toast.makeText(getContext(), "Showing Nearby "+locationSearch+" Place", Toast.LENGTH_SHORT).show();

    }

    private String getUrl(double latitude,double longitude, String nearbyPlace){

        mMap.clear();

        StringBuilder googlePlaceUrl= new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location="+latitude+","+longitude);
        googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type="+nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key="+"AIzaSyB05W-ViOZAo4LEHXXPXsRNl12DC0TURjE");

        Log.d("MapsActivity", "url = "+googlePlaceUrl.toString());

        return googlePlaceUrl.toString();
    }

}
