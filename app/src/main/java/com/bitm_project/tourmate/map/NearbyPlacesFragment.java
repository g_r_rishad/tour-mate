package com.bitm_project.tourmate.map;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bitm_project.tourmate.R;


public class NearbyPlacesFragment extends Fragment implements View.OnClickListener {


    private LinearLayout atmLayout,mosqueLayout,bankLayout,policeLayout,restaurentLayout,
            parkLayout,hospitalLayout,educationLayout,cafeLayout;

    public NearbyPlacesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_nearby_places, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        atmLayout= view.findViewById(R.id.atmLinearLayoutId);
        atmLayout.setOnClickListener(this);
        mosqueLayout= view.findViewById(R.id.mosqueLinearLayoutId);
        mosqueLayout.setOnClickListener(this);
        bankLayout= view.findViewById(R.id.bankLinearLayoutId);
        bankLayout.setOnClickListener(this);
        policeLayout=view.findViewById(R.id.policeLinearLayoutId);
        policeLayout.setOnClickListener(this);
        restaurentLayout= view.findViewById(R.id.restaurentLinearLayoutId);
        restaurentLayout.setOnClickListener(this);
        parkLayout= view.findViewById(R.id.parkLinearLayoutId);
        parkLayout.setOnClickListener(this);
        hospitalLayout= view.findViewById(R.id.hospitalLinearLayoutId);
        hospitalLayout.setOnClickListener(this);
        educationLayout= view.findViewById(R.id.educationLinearLayoutId);
        educationLayout.setOnClickListener(this);
        cafeLayout= view.findViewById(R.id.cafeLinearLayoutId);
        cafeLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //  intent;
        Bundle bundle= new Bundle();
        MapFragment mapFragment= new MapFragment();
        NearbyPlacesActivity mapActivity= (NearbyPlacesActivity) getActivity();

        switch (v.getId()){
            case R.id.atmLinearLayoutId:

                bundle.putString("search","atm");
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                mapFragment.setArguments(bundle);
                System.out.println();

                break;
            case R.id.mosqueLinearLayoutId:
                bundle.putString("search","mosque");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.bankLinearLayoutId:
                bundle.putString("search","bank");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.policeLinearLayoutId:

                bundle.putString("search","police");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.restaurentLinearLayoutId:
                bundle.putString("search","restaurant");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.parkLinearLayoutId:
                bundle.putString("search","park");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.hospitalLinearLayoutId:
                bundle.putString("search","hospital");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.educationLinearLayoutId:
                bundle.putString("search","university");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.cafeLinearLayoutId:
                bundle.putString("search","cafe");
                mapFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,mapFragment)
                        .addToBackStack(null).commit();
                break;

        }
    }
}
