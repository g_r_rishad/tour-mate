package com.bitm_project.tourmate;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bitm_project.tourmate.activities.SignInActivity;
import com.bitm_project.tourmate.activities.WeatherActivity;
import com.bitm_project.tourmate.adapter.TourInfoAdapter;
import com.bitm_project.tourmate.map.NearbyPlacesActivity;
import com.bitm_project.tourmate.model.AddTourModel;
import com.bitm_project.tourmate.util.LogoutC;
import com.bitm_project.tourmate.util.SharedPreferenceHandlerC;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private Toolbar toolbar;

    //
    private FloatingActionButton favBtn;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private Button saveTripBtn;
    private EditText tripDescription,tripName,startDateET,endDateET;
    private String description, name,startDate,endDate;
    private String fromDate, toDate,userId,uniqueKey;
    private long startDateMiliSec;
    private   Date date1,date;
    private RecyclerView recyclerView;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private List<AddTourModel> addTourModelList;
    private TourInfoAdapter tourInfoAdapter;
    private int tourCount=0;
    private SharedPreferenceHandlerC  handlerC;
    private  TextView titleTV;

    //
    private TextView historyTourName,historyDescription,historyStartDate,historyEndDate;
    private Button tourHistoryOkBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initView();
        retriveInfo();

    }

    @Override
    public void onBackPressed() {

     LogoutC.logoutProcess(MainActivity.this);

    }


    private void initView() {

        addTourModelList= new ArrayList<>();
        handlerC= new SharedPreferenceHandlerC(MainActivity.this);

        toolbar= findViewById(R.id.toolbarId);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tour Info");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        drawerLayout= findViewById(R.id.drawerId);
        toggle= new ActionBarDrawerToggle(this,drawerLayout,R.string.nav_open,R.string.nav_close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //
        navigationView= findViewById(R.id.navigationId);
        navigationView.setNavigationItemSelectedListener(this);

        //

        favBtn= findViewById(R.id.fab);

        favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPopupDialog();
            }
        });

        //recyclerView
        firebaseAuth= FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();

        recyclerView= findViewById(R.id.tourRecyclerViewId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    public void createPopupDialog(){
        dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.add_trip_layout,null);

        saveTripBtn= view.findViewById(R.id.saveTripButtonId);
        tripDescription= view.findViewById(R.id.tripDescriptionId);
        tripDescription.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        tripName = view.findViewById(R.id.tripNameId);
        startDateET= view.findViewById(R.id.tripStartDateId);
        startDateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerForStartDate();
            }
        });

        endDateET= view.findViewById(R.id.tripEndDateId);
        endDateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerForEndDate();
            }
        });


        dialogBuilder.setView(view);
        dialog = dialogBuilder.create();
        dialog.show();

        saveTripBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                description= tripDescription.getText().toString();
                name= tripName.getText().toString();
                startDate= startDateET.getText().toString();
                endDate= endDateET.getText().toString();

                saveTripInfo(description,name,startDate,endDate);

            }
        });

    }

    private boolean checkValidation(){

        if (name.matches("")){
            Toast.makeText(this, "Input Trip Name", Toast.LENGTH_SHORT).show();
            return  false;
        }else if (description.matches("")){
            Toast.makeText(this, "Input Description", Toast.LENGTH_SHORT).show();
            return false;
        }else if (startDate.matches("")){
            Toast.makeText(this, "Select Start Date", Toast.LENGTH_SHORT).show();
            return false;
        }else if (endDate.matches("")){
            Toast.makeText(this, "Select End Date", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void saveTripInfo(String description, String name, String startDate, String endDate) {


        if (checkValidation()){

            userId= firebaseAuth.getCurrentUser().getUid();

            DatabaseReference addTourRef=databaseReference.child("tourmate").child("tourHistory");

            uniqueKey=addTourRef.push().getKey();


            tourCount=Integer.valueOf(handlerC.getTourCount());
            tourCount=tourCount+1;
            handlerC.tourCount(tourCount);

            AddTourModel addTourModel= new AddTourModel(name,description,startDate,endDate,tourCount);

            addTourRef.child(userId).child("tour "+tourCount).child("details").setValue(addTourModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();

                        }
                    },1000);

                    Toast.makeText(MainActivity.this, "Successfully Added Tour", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(MainActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }

    }


    private void retriveInfo(){

        userId=firebaseAuth.getCurrentUser().getUid();

        DatabaseReference retriveRef= databaseReference.child("tourmate").child("tourHistory").child(userId);

        retriveRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                addTourModelList.clear();

                if (dataSnapshot.exists()){

                    for (DataSnapshot tourData: dataSnapshot.getChildren()){

                        AddTourModel  tourModel= tourData.child("details").getValue(AddTourModel.class);

                        addTourModelList.add(tourModel);
                    }

                    tourInfoAdapter= new TourInfoAdapter(MainActivity.this,addTourModelList);
                    recyclerView.setAdapter(tourInfoAdapter);
                    tourInfoAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(MainActivity.this, ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item)){

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        Intent intent;



        if (menuItem.getItemId()==R.id.neaByPlaceMenuId){

            intent= new Intent(this, NearbyPlacesActivity.class);
            startActivity(intent);
        }else if (menuItem.getItemId()==R.id.weatherMenuId){

            intent= new Intent(this, WeatherActivity.class);
            startActivity(intent);
        }else if (menuItem.getItemId()==R.id.logoutMenuId){
            LogoutC.logoutProcess(MainActivity.this);
        }else if (menuItem.getItemId()==R.id.homeMenuId){
            intent= new Intent(this,MainActivity.class);
            startActivity(intent);
        }

        return false;
    }

    private void openDatePickerForStartDate() {

        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                month = month + 1;
                String currentDate = dayOfMonth + "/" + month + "/" + year;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                 date = null;
                try {
                    date = dateFormat.parse(currentDate);
                    fromDate = dateFormat.format(date);
                    //startDateMiliSec = date.getTime();
                    // System.out.println("Date----------------------------------------------"+dateFormat.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                startDateET.setText(dateFormat.format(date));

            }
        };

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, dateSetListener, year, month, day);
        datePickerDialog.show();

    }

    private void openDatePickerForEndDate() {


        if (date == null) {


            Toast.makeText(this, "Select start date", Toast.LENGTH_SHORT).show();
        } else {


            DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                    month = month + 1;
                    String currentDate = dayOfMonth + "/" + month + "/" + year;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    date1 = null;
                    try {
                        date1 = dateFormat.parse(currentDate);
                        fromDate = dateFormat.format(date1);
                        startDateMiliSec = date1.getTime();
                        // System.out.println("Date----------------------------------------------"+dateFormat.format(date));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    endDateET.setText(dateFormat.format(date1));

                }
            };

            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, dateSetListener, year, month, day);


            datePickerDialog.getDatePicker().setMinDate(date.getTime());

            datePickerDialog.show();

        }
    }

    public void deleteTour(int tourCount) {

        userId = firebaseAuth.getCurrentUser().getUid();

        String tourNo= "tour "+String.valueOf(tourCount);

        final DatabaseReference deleteDataRef= databaseReference.child("tourmate").child("tourHistory").child(userId).child(tourNo);

        deleteDataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot delete : dataSnapshot.getChildren()){

                    deleteDataRef.removeValue();

                    Toast.makeText(MainActivity.this, "Tour Deleted Successfully", Toast.LENGTH_SHORT).show();
                  // tourInfoAdapter. notifyDataSetChanged();

                   // delete.getRef().setValue(null);
                //    tourInfoAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(MainActivity.this, ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void updateTourInfo(String tourName, String tourDescription, String startDate, String endDate, int tourCount) {

        updatePopupDialog(tourName,tourDescription,startDate,endDate,tourCount);
    }

    private void updatePopupDialog(final String tourName, String tourDescription, String tourStartDate, String tourEndDate, final int tourCount) {

        dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.add_trip_layout,null);

       titleTV= view.findViewById(R.id.titleTV);
        titleTV.setText("Update Tour Info");



        saveTripBtn= view.findViewById(R.id.saveTripButtonId);

        saveTripBtn.setText("Update Tour Info");
        tripDescription= view.findViewById(R.id.tripDescriptionId);
        tripDescription.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        tripName = view.findViewById(R.id.tripNameId);
        startDateET= view.findViewById(R.id.tripStartDateId);
        startDateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerForStartDate();
            }
        });
        endDateET= view.findViewById(R.id.tripEndDateId);
        endDateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerForEndDate();
            }
        });

        //
        tripName.setText(tourName);
        tripDescription.setText(tourDescription);
        startDateET.setText(tourStartDate);
        endDateET.setText(tourEndDate);


        dialogBuilder.setView(view);
        dialog = dialogBuilder.create();
        dialog.show();

        saveTripBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                description= tripDescription.getText().toString();
                name= tripName.getText().toString();
                startDate= startDateET.getText().toString();
                endDate= endDateET.getText().toString();

                if (checkValidation()){

                    updateTripInfo(description,name,startDate,endDate,tourCount);
                }




            }
        });
    }

    private void updateTripInfo(String description, String name, String startDate, String endDate, int tourCount) {

        userId= firebaseAuth.getCurrentUser().getUid();
        DatabaseReference updateDataRef= databaseReference.child("tourmate").child("tourHistory").child(userId).child("tour "+tourCount).child("details");


        AddTourModel updateTourInfo= new AddTourModel(name,description,startDate,endDate,tourCount);

        // String expenseId= objectRef.push().getKey();
        // expense.setExpenseId(expenseId);

        // objectRef.setValue(expense);



            updateDataRef.setValue(updateTourInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();

                        }
                    },1000);

                    if (task.isSuccessful()){
                        Toast.makeText(MainActivity.this, "Tour Info Update", Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(MainActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();
                    }
                }
            });







    }

    public void showTourHistory(String tourName, String tourDescription, String startDate, String endDate) {

        dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.tour_history_layout,null);


        historyTourName= view.findViewById(R.id.tourHistoryTourNameId);
        historyTourName.setText(tourName);
        historyDescription= view.findViewById(R.id.tourHistoryTourDescriptionId);
        historyDescription.setText(tourDescription);
        historyStartDate= view.findViewById(R.id.tourHistoryStartDateId);
        historyStartDate.setText(startDate);
        historyEndDate=view.findViewById(R.id.tourHistoryTourEndDateId);
        historyEndDate.setText(endDate);

        tourHistoryOkBtn= view.findViewById(R.id.tourHistoryOkButtonId);
        tourHistoryOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog.dismiss();
            }
        });

        dialogBuilder.setView(view);
        dialog = dialogBuilder.create();
        dialog.show();


    }
}
