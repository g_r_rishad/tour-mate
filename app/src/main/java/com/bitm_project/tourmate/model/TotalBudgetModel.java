package com.bitm_project.tourmate.model;

public class TotalBudgetModel {

    private String budgetName;
    private double budgetPrice;

    public TotalBudgetModel() {
    }

    public TotalBudgetModel(String budgetName, double budgetPrice) {
        this.budgetName = budgetName;
        this.budgetPrice = budgetPrice;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public double getBudgetPrice() {
        return budgetPrice;
    }

    public void setBudgetPrice(double budgetPrice) {
        this.budgetPrice = budgetPrice;
    }
}
