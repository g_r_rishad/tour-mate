package com.bitm_project.tourmate.model;

public class TotalExpenseModel {

    private String expenseName;
    private double expensePrice;

    public TotalExpenseModel() {
    }

    public TotalExpenseModel(String expenseName, double expensePrice) {
        this.expenseName = expenseName;
        this.expensePrice = expensePrice;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public double getExpensePrice() {
        return expensePrice;
    }

    public void setExpensePrice(double expensePrice) {
        this.expensePrice = expensePrice;
    }
}
