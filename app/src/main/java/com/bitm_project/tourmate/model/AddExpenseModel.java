package com.bitm_project.tourmate.model;

public class AddExpenseModel {

    private String expenseName;
    private double expensePrice;
    private int expenseCount;

    public AddExpenseModel() {
    }

    public AddExpenseModel(String expenseName, double expensePrice, int expenseCount) {
        this.expenseName = expenseName;
        this.expensePrice = expensePrice;
        this.expenseCount = expenseCount;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public double getExpensePrice() {
        return expensePrice;
    }

    public void setExpensePrice(double expensePrice) {
        this.expensePrice = expensePrice;
    }

    public int getExpenseCount() {
        return expenseCount;
    }

    public void setExpenseCount(int expenseCount) {
        this.expenseCount = expenseCount;
    }
}
