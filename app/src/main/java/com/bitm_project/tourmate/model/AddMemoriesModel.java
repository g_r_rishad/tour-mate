package com.bitm_project.tourmate.model;

public class AddMemoriesModel {


    private String memoryDescription;
    private String imageUrl;
    private String tourName;


    public AddMemoriesModel() {
    }

    public AddMemoriesModel(String memoryDescription, String imageUrl, String tourName) {
        this.memoryDescription = memoryDescription;
        this.imageUrl = imageUrl;
        this.tourName = tourName;
    }

    public String getMemoryDescription() {
        return memoryDescription;
    }

    public void setMemoryDescription(String memoryDescription) {
        this.memoryDescription = memoryDescription;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }
}
