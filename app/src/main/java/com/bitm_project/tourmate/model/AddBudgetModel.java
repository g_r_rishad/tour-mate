package com.bitm_project.tourmate.model;

public class AddBudgetModel {

    private String budgetName;
    private double budgetPrice;


    public AddBudgetModel() {
    }

    public AddBudgetModel(String budgetName, double budgetPrice) {
        this.budgetName = budgetName;
        this.budgetPrice = budgetPrice;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public double getBudgetPrice() {
        return budgetPrice;
    }

    public void setBudgetPrice(double budgetPrice) {
        this.budgetPrice = budgetPrice;
    }
}
