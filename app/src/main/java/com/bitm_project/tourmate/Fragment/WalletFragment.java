package com.bitm_project.tourmate.Fragment;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.activities.TourDetailsActivity;
import com.bitm_project.tourmate.adapter.ExpenseShowDetailsAdapter;
import com.bitm_project.tourmate.model.AddBudgetModel;
import com.bitm_project.tourmate.model.AddExpenseModel;
import com.bitm_project.tourmate.model.TotalBudgetModel;
import com.bitm_project.tourmate.model.TotalExpenseModel;
import com.bitm_project.tourmate.util.SharedPreferenceHandlerC;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class WalletFragment extends Fragment  {

    private FloatingActionButton addBudgetFabBtn, addExpenseFabBtn;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog  dialog;
    private EditText expenseOrBudgetNameET,expenseOrBudgetPriceET;
    private Button expenseOrBudgetCancelBtn, expenseOrBudgetSaveBtn;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference ;
    private String budgetOrExpenseName,budgetOrExpensePrice,expenseName,expensePrice,tourName;
    private int tourCount,budgetCount=0,expenseCount=0;
    private String userId;
    private SharedPreferenceHandlerC handlerC;
    private TextView titleTV,balanceTV,expenseTV,totalBudgetTV,expensePercentageTV,budgetPercentageTV;
    double totalBudget=0,totalExpense=0;
    double inputBudget=0,inputExpense=0;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private LinearLayout seeDetailsAndBudgetLayout;


    // details part
    private FrameLayout frameLayout;
    private TabLayout tabLayout;



    public WalletFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        handlerC= new SharedPreferenceHandlerC(getActivity());
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_wallet, container, false);

        tourCount= TourDetailsActivity.tourCount;
        tourName=TourDetailsActivity.tourName;

        intView(view);



        getTotalBudgetSummaryInfo();
        getTotalExpenseSummaryInfo();
        updateBudgetAndExpenseInfo();
        return view;
    }

    private void intView(View view) {


        toolbar= view.findViewById(R.id.toolbarId);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Tour Account");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) getActivity()).onBackPressed();
            }
        });
        //
        firebaseAuth= FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();

        //
        balanceTV= view.findViewById(R.id.balanceTkId);
        expenseTV= view.findViewById(R.id.expenseTkId);
        totalBudgetTV=view.findViewById(R.id.totalBudgetId);
        expensePercentageTV= view.findViewById(R.id.expensePercentageId);
        budgetPercentageTV= view.findViewById(R.id.budgetPercentageId);
        progressBar= view.findViewById(R.id.stats_progressbar);

        addBudgetFabBtn= view.findViewById(R.id.addBudgetFloatingActionButtonId);
        addBudgetFabBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAddBudgetPopUp();
            }
        });
        addExpenseFabBtn= view.findViewById(R.id.addExpenseFloatingActionButtonId);
        addExpenseFabBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAddExpensePopUp();
            }
        });


        seeDetailsAndBudgetLayout= view.findViewById(R.id.seeDetailsBudgetAndExpenseLinearLayoutId);
        seeDetailsAndBudgetLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BudgetAndExpenseDetailsFragment budgetAndExpenseDetailsFragment= new BudgetAndExpenseDetailsFragment();

                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                FragmentTransaction transaction= fragmentManager.beginTransaction();
                transaction.replace(R.id.frameLayoutId,budgetAndExpenseDetailsFragment);

                transaction.commit();
            }
        });


    }


    private void createAddBudgetPopUp(){


        dialogBuilder= new AlertDialog.Builder(getContext());

        LayoutInflater inflater= LayoutInflater.from(getContext());
        View view= inflater.inflate(R.layout.add_budget_layout,null);

        titleTV= view.findViewById(R.id.addExpenseOrBudgetTitleId);
        titleTV.setText("Add Budget");
        //
        expenseOrBudgetNameET= view.findViewById(R.id.budgetCategoryNameId);
        expenseOrBudgetPriceET= view.findViewById(R.id.budgetCategoryPriceId);
        expenseOrBudgetSaveBtn=view.findViewById(R.id.saveBudgetOrExpenseButtonId);
        expenseOrBudgetSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveBudgetInfo();
            }
        });

        expenseOrBudgetCancelBtn= view.findViewById(R.id.cancelBudgetOrExpenseButtonId);
        expenseOrBudgetCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialogBuilder.setView(view);
        dialog= dialogBuilder.create();
        dialog.show();

    }

    private void createAddExpensePopUp(){


        dialogBuilder= new AlertDialog.Builder(getContext());

        LayoutInflater inflater= LayoutInflater.from(getContext());
        View view= inflater.inflate(R.layout.add_budget_layout,null);

        titleTV= view.findViewById(R.id.addExpenseOrBudgetTitleId);
        titleTV.setText("Add Expense");

        //
        expenseOrBudgetNameET= view.findViewById(R.id.budgetCategoryNameId);
        expenseOrBudgetPriceET= view.findViewById(R.id.budgetCategoryPriceId);
        expenseOrBudgetSaveBtn=view.findViewById(R.id.saveBudgetOrExpenseButtonId);
        expenseOrBudgetSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveExpensenfo();
            }
        });

        expenseOrBudgetCancelBtn= view.findViewById(R.id.cancelBudgetOrExpenseButtonId);
        expenseOrBudgetCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialogBuilder.setView(view);
        dialog= dialogBuilder.create();
        dialog.show();

    }

    public void saveBudgetInfo() {


        userId=firebaseAuth.getCurrentUser().getUid();

        budgetCount=Integer.valueOf(handlerC.getBudgetCount());
        budgetCount=budgetCount+1;
        handlerC.budgetCount(budgetCount);

        if (checkValidation()){


            totalBudget= Double.valueOf(handlerC.getTotalBudget());


            inputBudget=Double.valueOf(budgetOrExpensePrice);
            totalBudget= totalBudget+inputBudget;

            totalBudgetSummaryInfo(totalBudget);
          //  handlerC.setTotalBudget(totalBudget);


            DatabaseReference saveBudgetInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                    child("tour "+tourCount).child("tourAccount").child("budgetInfo").child("budget "+budgetCount);


            AddBudgetModel budgetModel= new AddBudgetModel(budgetOrExpenseName,Double.valueOf(budgetOrExpensePrice));

            saveBudgetInfoRef.setValue(budgetModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();

                        }
                    },1000);

                    if (task.isSuccessful()){
                        Toast.makeText(getContext(), "Budget Added Successfully", Toast.LENGTH_SHORT).show();
                        updateBudgetAndExpenseInfo();
                    }else{
                        Toast.makeText(getContext(), ""+task.getException(), Toast.LENGTH_SHORT).show();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void totalBudgetSummaryInfo(double totalBudget){

        userId= firebaseAuth.getCurrentUser().getUid();

        DatabaseReference saveTotalBudgetInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour "+tourCount).child("tourAccount").child("budgetSummaryInfo");

        TotalBudgetModel budgetModel= new TotalBudgetModel("Total Budget",totalBudget);

        saveTotalBudgetInfoRef.setValue(budgetModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                    Toast.makeText(getContext(), "Total Budget Saved !!", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }


    private void totalExpenseSummaryInfo(double expenseBudget){

        userId= firebaseAuth.getCurrentUser().getUid();

        DatabaseReference saveTotalBudgetInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour "+tourCount).child("tourAccount").child("expenseSummaryInfo");

        TotalExpenseModel expenseModel= new TotalExpenseModel("Total Expense",expenseBudget);

        saveTotalBudgetInfoRef.setValue(expenseModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                   // Toast.makeText(getContext(), "Total Expense Saved !!", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }


    private void getTotalExpenseSummaryInfo(){

        userId= firebaseAuth.getCurrentUser().getUid();

        DatabaseReference getTotalBudgetInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour "+tourCount).child("tourAccount").child("expenseSummaryInfo");

        getTotalBudgetInfoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    totalExpense=Double.valueOf(dataSnapshot.child("expensePrice").getValue().toString());
                    //balanceTV.setText(String.valueOf(totalBudget));
                    handlerC.setTotalExpense(totalExpense);
                    updateBudgetAndExpenseInfo();


                }else {

                    totalExpense=0;
                    // balanceTV.setText(String.valueOf(totalBudget));
                    handlerC.setTotalExpense(totalExpense);
                    updateBudgetAndExpenseInfo();


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getContext(), ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void getTotalBudgetSummaryInfo(){

        userId= firebaseAuth.getCurrentUser().getUid();

        DatabaseReference getTotalBudgetInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour "+tourCount).child("tourAccount").child("budgetSummaryInfo");

        getTotalBudgetInfoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    totalBudget=Double.valueOf(dataSnapshot.child("budgetPrice").getValue().toString());
                    //balanceTV.setText(String.valueOf(totalBudget));
                    handlerC.setTotalBudget(totalBudget);
                    updateBudgetAndExpenseInfo();



                }else {

                    totalBudget=0;
                   // balanceTV.setText(String.valueOf(totalBudget));
                    handlerC.setTotalBudget(totalBudget);
                    updateBudgetAndExpenseInfo();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getContext(), ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void updateBudgetAndExpenseInfo(){


        double balance=0;
        double ex=Double.valueOf(handlerC.getTotalBudget());

        balance= Double.valueOf(handlerC.getTotalBudget()) - Double.valueOf(handlerC.getTotalExpense());


        totalBudgetTV.setText("Total Budget : "+String.valueOf(handlerC.getTotalBudget()));
        balanceTV.setText(String.valueOf(balance));

        expenseTV.setText(handlerC.getTotalExpense());

        //

        double d= (double) balance /(double)  ex  ;

        int progress=0;
         progress=(int)(d*100);

        System.out.println("-----------------progress------------------------------"+progress);

        int expenseBal= 100- progress;
        expensePercentageTV.setText(" - "+String.valueOf(expenseBal) +" % ");
        budgetPercentageTV.setText(" - "+String.valueOf(progress)+ " % ");



        progressBar.setProgress(progress);

    }

    private void saveExpensenfo() {


        userId=firebaseAuth.getCurrentUser().getUid();

        expenseCount=Integer.valueOf(handlerC.getExpenseCount());
        expenseCount=expenseCount+1;
        handlerC.expenseCount(expenseCount);


        if (checkValidation()){


            totalExpense= Double.valueOf(handlerC.getTotalExpense());
            inputExpense= Double.valueOf(budgetOrExpensePrice);
            totalExpense= totalExpense+inputExpense;
            totalExpenseSummaryInfo(totalExpense);

          //  handlerC.setTotalExpense(totalExpense);

            DatabaseReference saveExpenseInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                    child("tour "+tourCount).child("tourAccount").child("expenseInfo").child("expense "+expenseCount);


            AddExpenseModel expenseModel= new AddExpenseModel(budgetOrExpenseName,Double.valueOf(budgetOrExpensePrice),expenseCount);

            saveExpenseInfoRef.setValue(expenseModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();

                        }
                    },1000);

                    if (task.isSuccessful()){
                       // Toast.makeText(getContext(), "Expense Added Successfully", Toast.LENGTH_SHORT).show();
                        updateBudgetAndExpenseInfo();
                    }else{
                        Toast.makeText(getContext(), ""+task.getException(), Toast.LENGTH_SHORT).show();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });


        }

    }


    private boolean checkValidation(){

        budgetOrExpenseName= expenseOrBudgetNameET.getText().toString();
        budgetOrExpensePrice= expenseOrBudgetPriceET.getText().toString();

        if (budgetOrExpenseName.matches("")){
            Toast.makeText(getContext(), "Enter Budget Name", Toast.LENGTH_SHORT).show();
            return false;
        }else if (budgetOrExpensePrice.matches("")){
            Toast.makeText(getContext(), "Enter Budget Price", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

}
