package com.bitm_project.tourmate.Fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.activities.TourDetailsActivity;
import com.bitm_project.tourmate.adapter.BudgetShowDetailsAdapter;
import com.bitm_project.tourmate.model.AddBudgetModel;
import com.bitm_project.tourmate.model.AddExpenseModel;
import com.bitm_project.tourmate.model.TotalBudgetModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BudgetDetailsFragment extends Fragment {


    private RecyclerView budgetDetailsRecyclerView;
    private List<AddBudgetModel> AddBudgetModelList;
    private BudgetShowDetailsAdapter budgetShowDetailsAdapter;
    private String userId;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private int tourCount;
    private String tourName;


    public BudgetDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        tourCount= TourDetailsActivity.tourCount;
        tourName=TourDetailsActivity.tourName;

        View view= inflater.inflate(R.layout.fragment_budget_details, container, false);

        initView(view);
        showBudgetDetails();
        return view;
    }

    private void initView(View view) {

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();

        AddBudgetModelList= new ArrayList<>();

        budgetDetailsRecyclerView= view.findViewById(R.id.budgetDetailsRecyclerViewId);
        budgetDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void showBudgetDetails(){

        userId= firebaseAuth.getCurrentUser().getUid();

        DatabaseReference saveTotalBudgetInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour "+tourCount).child("tourAccount").child("budgetInfo");

        saveTotalBudgetInfoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    for (DataSnapshot budgetData: dataSnapshot.getChildren()) {

                      //  TotalBudgetModel totalBudgetModel =budgetData.getValue(TotalBudgetModel.class);
                      AddBudgetModel addBudgetModel =budgetData.getValue(AddBudgetModel.class);

                        AddBudgetModelList.add(addBudgetModel);
                    }

                    budgetShowDetailsAdapter= new BudgetShowDetailsAdapter(AddBudgetModelList,getContext());
                    budgetDetailsRecyclerView.setAdapter(budgetShowDetailsAdapter);
                    budgetShowDetailsAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
