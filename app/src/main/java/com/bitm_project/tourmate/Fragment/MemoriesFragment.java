package com.bitm_project.tourmate.Fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.activities.SignUpActivity;
import com.bitm_project.tourmate.activities.TourDetailsActivity;
import com.bitm_project.tourmate.adapter.TourMomentAdapter;
import com.bitm_project.tourmate.model.AddMemoriesModel;
import com.bitm_project.tourmate.util.SharedPreferenceHandlerC;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;


public class MemoriesFragment extends Fragment {


    private FloatingActionButton addMemoriesFabBtn, cameraFabBtn;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private Bitmap bitmapImage= null;
    private EditText memoryDescriptionET;
    private String memoryDescription,downloadLink,picSource,userId;
    private Button saveMemoryBtn;
    private ImageView tripImage;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;
    private Uri selectedImage;
    @SuppressWarnings("VisibleForTests")
    private AddMemoriesModel  addMemoriesModel;
    private byte[] aa;
    private String tourName;
    private int tourCount;
    private RecyclerView memoriesRecyclerView;
    private List<AddMemoriesModel> addMemoriesModelList;
    private TourMomentAdapter tourMomentAdapter;
    private int memoryCount=0;
    private SharedPreferenceHandlerC handlerC;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;


    public MemoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        progressDialog= new ProgressDialog(getContext());
        handlerC= new SharedPreferenceHandlerC(getActivity());
        addMemoriesModelList= new ArrayList<>();


        tourName= TourDetailsActivity.tourName;
        tourCount= TourDetailsActivity.tourCount;

        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_memories, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {


        toolbar= view.findViewById(R.id.toolbarId);
        toolbar= view.findViewById(R.id.toolbarId);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Tour Moment");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) getActivity()).onBackPressed();
            }
        });

        //
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();
        storageReference= FirebaseStorage.getInstance().getReference();
        showMemoriesData();
        //

        memoriesRecyclerView=view.findViewById(R.id.memoriesRecyclerViewId);
        memoriesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        addMemoriesFabBtn= view.findViewById(R.id.addMemoriesFab);
        addMemoriesFabBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createMemories();
            }
        });

    }

    private void createMemories() {

        dialogBuilder= new AlertDialog.Builder(getContext());

        View view = getLayoutInflater().inflate(R.layout.add_memories_layout,null);
        //
        tripImage= view.findViewById(R.id.imageId);
        cameraFabBtn= view.findViewById(R.id.imageFabButtonId);
        memoryDescriptionET= view.findViewById(R.id.tourTripDescriptionId);

        saveMemoryBtn= view.findViewById(R.id.saveMemoriesButtonId);

        saveMemoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                memoryDescription=memoryDescriptionET.getText().toString();

                if (picSource == null ){
                    Toast.makeText(getContext(), "Choose Image", Toast.LENGTH_SHORT).show();
                    return;
                }else if (memoryDescription.matches("")){
                    Toast.makeText(getContext(), "Write Description", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    upLoadImage();
                }


            }
        });

//        saveMemoryBtn.setOnClickListener(this);

        cameraFabBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(getContext(), "Clik", Toast.LENGTH_SHORT).show();
                openPicDialog();
            }
        });

        dialogBuilder.setView(view);
        dialog= dialogBuilder.create();
        dialog.show();

    }

    private void openPicDialog(){
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        String[] items = {"From Camera","From Gallery"};
        builder.setTitle("Choose an action");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        openCamera();
                        break;
                    case 1:
                        openGallery();
                        break;
                }
            }
        });

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent,99);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,88);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(resultCode == RESULT_OK){
            if(requestCode == 88) {

                if (data != null) {

                    picSource = "camera";

                    Bundle bundle = data.getExtras();
                    bitmapImage = (Bitmap) bundle.get("data");
                    // selectedImage=data.getData();
                    tripImage.setImageBitmap(bitmapImage);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    aa = baos.toByteArray();
                }else {

                    Toast.makeText(getContext(), "Select One", Toast.LENGTH_SHORT).show();
                }
            }
            else if(requestCode == 99){
                    if (data != null){
                        picSource="gallery";
                        selectedImage= data.getData();

                      //  tripImage.setImageURI(selectedImage);

                        try {
                            Bitmap bitmap= MediaStore.Images.Media.getBitmap(getContext().getApplicationContext().getContentResolver(), selectedImage);
                            tripImage.setImageBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // useImageUri(selectedImage);

                    }else {
                        Toast.makeText(getContext(), "Select Image", Toast.LENGTH_SHORT).show();
                        return;
                    }
            }
        }
        }

    private void upLoadImage(){

        StorageReference memoriesImageRef= storageReference.child("uploadMemories -"+ UUID.randomUUID());

        progressDialog.setMessage("Please Wait..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        if (picSource.matches("camera")) {

            memoriesImageRef.putBytes(aa).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if (task.isSuccessful()){
                        Toast.makeText(getActivity(), "Upload Successfully", Toast.LENGTH_SHORT).show();
                        addMemories();
                    }else {
                        Toast.makeText(getActivity(), ""+task.getException(), Toast.LENGTH_SHORT).show();
                    }


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();
                    while (!uri.isComplete()) ;
                    Uri url = uri.getResult();

                    downloadLink = url.toString();

                  //  System.out.println(".................................................................." + downloadLink);

                    //       Picasso.get().load(url).into(showImage);

                }
            });

        } else if (picSource.matches("gallery")) {

            memoriesImageRef.putFile(selectedImage).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {


                    if (task.isSuccessful()) {

                      //  Toast.makeText(getContext(), "Upload Successfully", Toast.LENGTH_SHORT).show();
                        addMemories();
                    }else {
                        Toast.makeText(getContext(), ""+task.getException(), Toast.LENGTH_SHORT).show();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                   // downloadLink = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!urlTask.isSuccessful());
                    Uri downloadUrl = urlTask.getResult();
                    downloadLink=downloadUrl.toString();


                    //Picasso.get().load(downloadLink).into(showImage);
                }
            });
        }
    }

    private void addMemories(){



      userId=firebaseAuth.getCurrentUser().getUid();

        memoryCount=Integer.valueOf(handlerC.getMemoryCount());

        System.out.println(".....................................Memory count"+memoryCount);
        memoryCount=memoryCount+1;
        handlerC.memoryCount(memoryCount);

      DatabaseReference addMemoriesRef= databaseReference.child("tourmate").child("tourHistory").child(userId).child("tour "+tourCount).
              child("tourMemory").child("memory "+memoryCount);


      AddMemoriesModel memoriesModel= new AddMemoriesModel(memoryDescription,downloadLink,tourName);

      addMemoriesRef.setValue(memoriesModel).addOnCompleteListener(new OnCompleteListener<Void>() {
          @Override
          public void onComplete(@NonNull Task<Void> task) {

              if (task.isSuccessful()){
                  progressDialog.dismiss();
                  new Handler().postDelayed(new Runnable() {
                      @Override
                      public void run() {
                          dialog.dismiss();

                      }
                  },1000);
                  Toast.makeText(getContext(), "Tour moment added", Toast.LENGTH_SHORT).show();
              }

          }
      }).addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
              progressDialog.dismiss();

              Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
          }
      });

    }

    private void showMemoriesData(){


        String uId=firebaseAuth.getCurrentUser().getUid();

        DatabaseReference retriveMemoryRef= databaseReference.child("tourmate").child("tourHistory").child(uId).
                child("tour "+tourCount).child("tourMemory");

      //  progressDialog.setMessage("Please wait...");
      //  progressDialog.setCanceledOnTouchOutside(false);
      //  progressDialog.show();

        retriveMemoryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                addMemoriesModelList.clear();

                if (dataSnapshot != null){

                    for (DataSnapshot memoryData : dataSnapshot.getChildren()){
                        AddMemoriesModel memoriesModel=memoryData.getValue(AddMemoriesModel.class);

                        addMemoriesModelList.add(memoriesModel);

                    }

                    tourMomentAdapter= new TourMomentAdapter(getActivity(),addMemoriesModelList);
                    memoriesRecyclerView.setAdapter(tourMomentAdapter);
                    tourMomentAdapter.notifyDataSetChanged();

                   // progressDialog.dismiss();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getActivity(), ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
               // progressDialog.dismiss();
            }
        });


    }

    private boolean checkValidation(){





        if (tripImage== null && selectedImage == null){
            Toast.makeText(getContext(), "Choose Image", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }


}
