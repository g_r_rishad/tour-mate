package com.bitm_project.tourmate.Fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.activities.TourDetailsActivity;
import com.bitm_project.tourmate.adapter.BudgetShowDetailsAdapter;
import com.bitm_project.tourmate.adapter.ExpenseShowDetailsAdapter;
import com.bitm_project.tourmate.model.AddExpenseModel;
import com.bitm_project.tourmate.model.TotalBudgetModel;
import com.bitm_project.tourmate.model.TotalExpenseModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpenseDetailsFragment extends Fragment {

    private RecyclerView expenseDetailsRecyclerView;
    private List<AddExpenseModel> totalExpenseModelList;
    private ExpenseShowDetailsAdapter expenseShowDetailsAdapter;
    private String userId;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private int tourCount;
    private String tourName;




    public ExpenseDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        tourCount= TourDetailsActivity.tourCount;
        tourName=TourDetailsActivity.tourName;


        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_expense_details, container, false);
        initView(view);
        showExpenseDetails();
        return view;
    }

    private void initView(View view) {

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();

        totalExpenseModelList= new ArrayList<>();

        expenseDetailsRecyclerView= view.findViewById(R.id.expenseDetailsRecyclerViewId);
        expenseDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void showExpenseDetails(){

        userId= firebaseAuth.getCurrentUser().getUid();

        totalExpenseModelList.removeAll(totalExpenseModelList);

        DatabaseReference getTotalExpenseData= databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour "+tourCount).child("tourAccount").child("expenseInfo");

        getTotalExpenseData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    for (DataSnapshot expenseData: dataSnapshot.getChildren()) {

            //            TotalExpenseModel totalExpenseModel =expenseData.getValue(TotalExpenseModel.class);
                        AddExpenseModel addExpenseModel =expenseData.getValue(AddExpenseModel.class);

                        totalExpenseModelList.add(addExpenseModel);
                    }

                    expenseShowDetailsAdapter= new ExpenseShowDetailsAdapter(totalExpenseModelList,getContext(),(AppCompatActivity) getActivity());
                    expenseDetailsRecyclerView.setAdapter(expenseShowDetailsAdapter);
                    expenseShowDetailsAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getContext(), ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
