package com.bitm_project.tourmate.Fragment;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bitm_project.tourmate.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class BudgetAndExpenseDetailsFragment extends Fragment {


    private Toolbar toolbar;
    private FrameLayout frameLayout;
    private TabLayout tabLayout;


    public BudgetAndExpenseDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        FragmentManager fragmentManager= getFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        transaction.replace(R.id.budgetAndSalaryDetailsFrameLayoutId,new BudgetDetailsFragment());
        transaction.commit();


        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_budget_and_expense_details, container, false);
        initView(view);
       // replaceFragment(new BudgetDetailsFragment());
        return view;
    }

    private void initView(View view) {



        toolbar= view.findViewById(R.id.toolbarId);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Budget And Expense Details");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity)getActivity()).onBackPressed();
            }
        });

        tabLayout= view.findViewById(R.id.budgetAndExpenseDetailsTabLayoutId);
        frameLayout= view.findViewById(R.id.budgetAndSalaryDetailsFrameLayoutId);



        tabLayout.addTab(tabLayout.newTab().setText("Budget Details"));
        tabLayout.addTab(tabLayout.newTab().setText("Expense Details"));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition()==0){
                    replaceFragment(new BudgetDetailsFragment());

                }else if (tab.getPosition()==1){
                    replaceFragment(new ExpenseDetailsFragment());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void replaceFragment(Fragment fragment){

        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        transaction.replace(R.id.budgetAndSalaryDetailsFrameLayoutId,fragment);

        transaction.addToBackStack(null).commit();

    }

}
