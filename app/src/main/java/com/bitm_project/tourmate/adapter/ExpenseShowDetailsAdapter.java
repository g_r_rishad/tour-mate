package com.bitm_project.tourmate.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bitm_project.tourmate.Fragment.BudgetDetailsFragment;
import com.bitm_project.tourmate.Fragment.ExpenseDetailsFragment;
import com.bitm_project.tourmate.Fragment.WalletFragment;
import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.activities.UpdateExDetails;
import com.bitm_project.tourmate.activities.UpdateExpenseOrBudgetInfoActivity;
import com.bitm_project.tourmate.model.AddExpenseModel;
import com.bitm_project.tourmate.model.TotalBudgetModel;
import com.bitm_project.tourmate.model.TotalExpenseModel;
import com.bitm_project.tourmate.util.SharedPreferenceHandlerC;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ExpenseShowDetailsAdapter extends RecyclerView.Adapter<ExpenseShowDetailsAdapter.MyViewHolder> {


    private List<AddExpenseModel> totalExpenseModelList;
    private Context context;
    private AppCompatActivity appCompatActivity;

    public ExpenseShowDetailsAdapter(List<AddExpenseModel> totalExpenseModelList, Context context, AppCompatActivity appCompatActivity) {
        this.totalExpenseModelList = totalExpenseModelList;
        this.context = context;
        this.appCompatActivity= appCompatActivity;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater= LayoutInflater.from(context);
        View view= inflater.inflate(R.layout.recycler_expense_or_budget_item_design,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

      final   UpdateExDetails updateExDetails= new UpdateExDetails(appCompatActivity);

        final AddExpenseModel expenseModel= totalExpenseModelList.get(position);

        holder.budgetOrExpenseNameTV.setText(expenseModel.getExpenseName());
        holder.budgetOrExpensePriceTV.setText(String.valueOf(expenseModel.getExpensePrice()));
        holder.budgetOrExpenseEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                double expensePrice= expenseModel.getExpensePrice();
                String expenseName= expenseModel.getExpenseName();
                int expenseCount= expenseModel.getExpenseCount();

               updateExDetails.updateExpenseInfo(expenseName,expensePrice,expenseCount);

              // updateExDetails.refresh();


              // Toast.makeText(context, "Edit Button click"+expenseCount, Toast.LENGTH_SHORT).show();
            }
        });

        holder.budgetOrExpenseDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                double expensePrice= expenseModel.getExpensePrice();
                String expenseName= expenseModel.getExpenseName();
                int expenseCount= expenseModel.getExpenseCount();

                updateExDetails.deleteExpenseInfo(expenseName,expensePrice,expenseCount);
               // Toast.makeText(context, "Delete Button Click", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return totalExpenseModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView budgetOrExpenseNameTV, budgetOrExpensePriceTV;
        private Button budgetOrExpenseEditBtn,budgetOrExpenseDeleteBtn;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            budgetOrExpenseNameTV=itemView.findViewById(R.id.recyclerBudgetOrExpenseNameId);
            budgetOrExpensePriceTV= itemView.findViewById(R.id.recyclerBudgetOrExpensePriceId);
            budgetOrExpenseEditBtn= itemView.findViewById(R.id.recyclerBudgetOrExpenseEditButtonId);
            budgetOrExpenseDeleteBtn= itemView.findViewById(R.id.recyclerBudgetOrExpenseDeleteButtonId);

        }
    }



    //



}
