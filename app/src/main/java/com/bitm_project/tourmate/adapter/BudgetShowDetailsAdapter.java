package com.bitm_project.tourmate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.model.AddBudgetModel;
import com.bitm_project.tourmate.model.TotalBudgetModel;

import java.util.List;

public class BudgetShowDetailsAdapter extends RecyclerView.Adapter<BudgetShowDetailsAdapter.MyViewHolder> {

    private List<AddBudgetModel> addBudgetModelList;
    private Context context;

    public BudgetShowDetailsAdapter(List<AddBudgetModel> addBudgetModelList, Context context) {
        this.addBudgetModelList = addBudgetModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater= LayoutInflater.from(context);
        View view= inflater.inflate(R.layout.recycler_budget_item_design,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

       final AddBudgetModel budgetModel= addBudgetModelList.get(position);

        holder.budgetOrExpenseNameTV.setText(budgetModel.getBudgetName());
        holder.budgetOrExpensePriceTV.setText(String.valueOf(budgetModel.getBudgetPrice()));


    }

    @Override
    public int getItemCount() {
        return addBudgetModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView budgetOrExpenseNameTV, budgetOrExpensePriceTV;
        private Button budgetOrExpenseEditBtn,budgetOrExpenseDeleteBtn;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            budgetOrExpenseNameTV=itemView.findViewById(R.id.recyclerBudgetOrExpenseNameId);
            budgetOrExpensePriceTV= itemView.findViewById(R.id.recyclerBudgetOrExpensePriceId);
          //  budgetOrExpenseEditBtn= itemView.findViewById(R.id.recyclerBudgetOrExpenseEditButtonId);
          //  budgetOrExpenseDeleteBtn= itemView.findViewById(R.id.recyclerBudgetOrExpenseDeleteButtonId);

        }
    }
}
