package com.bitm_project.tourmate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.model.AddMemoriesModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TourMomentAdapter extends RecyclerView.Adapter<TourMomentAdapter.MyViewHolder> {

    private Context context;
    private List<AddMemoriesModel> addMemoriesModelList;

    public TourMomentAdapter(Context context, List<AddMemoriesModel> addMemoriesModelList) {
        this.context = context;
        this.addMemoriesModelList = addMemoriesModelList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater  inflater= LayoutInflater.from(context);
        View view= inflater.inflate(R.layout.recycler_tour_moment_item_design,parent,false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        AddMemoriesModel memoriesModel= addMemoriesModelList.get(position);

        holder.tourDescriptionTV.setText(memoriesModel.getMemoryDescription());
        holder.tourNameTV.setText(memoriesModel.getTourName());

        System.out.println("URL-----------------------------------------------------"+memoriesModel.getImageUrl());



        Picasso.get().load(memoriesModel.getImageUrl()).into(holder.tourMemoriesImg);

    }

    @Override
    public int getItemCount() {
        return addMemoriesModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView tourMemoriesImg;
        private TextView tourNameTV,tourDescriptionTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tourMemoriesImg= itemView.findViewById(R.id.tourMomentImageId);
            tourNameTV= itemView.findViewById(R.id.tourNameTVId);
            tourDescriptionTV= itemView.findViewById(R.id.tourDescriptionTVId);

        }
    }
}
