package com.bitm_project.tourmate.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bitm_project.tourmate.MainActivity;
import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.activities.TourDetailsActivity;
import com.bitm_project.tourmate.model.AddTourModel;

import java.util.List;

public class TourInfoAdapter  extends RecyclerView.Adapter<TourInfoAdapter.MyViewHolder> {

    private Context context;
    private List<AddTourModel> addTourModelList;

    public TourInfoAdapter(Context context, List<AddTourModel> addTourModelList) {
        this.context = context;
        this.addTourModelList = addTourModelList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(context);
        View view= inflater.inflate(R.layout.recycler_tour_info_item_design,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final AddTourModel tourModel= addTourModelList.get(position);

        holder.tourNameTV.setText(tourModel.getTourName());
        holder.descriptionTV.setText(tourModel.getTourDescription());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tourName= tourModel.getTourName();
                String tourDescription= tourModel.getTourDescription();
                String startDate= tourModel.getStartDate();
                String endDate= tourModel.getEndDate();

                ((MainActivity)context).showTourHistory(tourName,tourDescription,startDate,endDate);

            }
        });


        holder.deleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tourCount = tourModel.getTourCount();

                ((MainActivity)context).deleteTour(tourCount);
               notifyDataSetChanged();
            }
        });

        holder.editTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tourName= tourModel.getTourName();
                String tourDescription= tourModel.getTourDescription();
                String startDate= tourModel.getStartDate();
                String endDate= tourModel.getEndDate();
                int tourCount= tourModel.getTourCount();

                ((MainActivity)context).updateTourInfo(tourName,tourDescription,startDate,endDate,tourCount);
            }
        });

        holder.detailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tourName= tourModel.getTourName();
                int tourCount= tourModel.getTourCount();

                Intent intent;
                intent = new Intent(context, TourDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("tourName",tourName);
                bundle.putInt("tourCount",tourCount);

                intent.putExtras(bundle);

                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return addTourModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tourNameTV,descriptionTV,detailsTV,editTV,deleteTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tourNameTV= itemView.findViewById(R.id.tourNameId);
            descriptionTV= itemView.findViewById(R.id.tourDescriptionId);
            detailsTV= itemView.findViewById(R.id.tourDetailsId);
            editTV= itemView.findViewById(R.id.tourEditId);
            deleteTV= itemView.findViewById(R.id.tourDeleteId);


        }
    }
}
