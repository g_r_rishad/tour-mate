package com.bitm_project.tourmate.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceHandlerC {

    private SharedPreferences sp;
    private Context ctx;
    private String userId;
    private String budget;
    private String expense;


    int tourCount,memoryCount,budgetCount,expenseCount;
    double totalBudget,expenseBudget;

    public SharedPreferenceHandlerC(Context ctx) {
        this.ctx = ctx;
        sp = ctx.getSharedPreferences("User", Context.MODE_PRIVATE);
    }

    public void tourCount(int tourCount) {
        this.tourCount = tourCount;
        saveToSp();
    }

    private void saveToSp() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("tourCount", String.valueOf(tourCount));
        editor.apply();
    }

    public String getTourCount() {

        return sp.getString("tourCount", "0");
    }

    // memory count
    public void memoryCount(int memoryCount) {
        this.memoryCount = memoryCount;
        saveToSpMemoryCount();
    }

    private void saveToSpMemoryCount() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("memoryCount", String.valueOf(memoryCount));
        editor.apply();
    }

    public String getMemoryCount() {

        return sp.getString("memoryCount", "0");
    }

    // budget count
    public void budgetCount(int budgetCount) {
        this.budgetCount = budgetCount;
        saveToSpBudget();
    }

    private void saveToSpBudget() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("budgetCount", String.valueOf(budgetCount));
        editor.apply();
    }

    public String getBudgetCount() {

        return sp.getString("budgetCount", "0");
    }


    // Expense count
    public void expenseCount(int expenseCount) {
        this.expenseCount = expenseCount;
        saveToSpExpense();
    }

    private void saveToSpExpense() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("expenseCount", String.valueOf(expenseCount));
        editor.apply();
    }

    public String getExpenseCount() {

        return sp.getString("expenseCount", "0");
    }


    // total budget
    public void setTotalBudget(double totalBudget) {
        this.totalBudget = totalBudget;
        saveToSpTotalBudget();
    }

    private void saveToSpTotalBudget() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("totalBudget", String.valueOf(totalBudget));
        editor.apply();
    }

    public String getTotalBudget() {

        return sp.getString("totalBudget", "0");
    }

    // total expense
    public void setTotalExpense(double expenseBudget) {
        this.expenseBudget = expenseBudget;
        saveToSpTotalExpense();
    }

    private void saveToSpTotalExpense() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("expenseBudget", String.valueOf(expenseBudget));
        editor.apply();
    }

    public String getTotalExpense() {

        return sp.getString("expenseBudget", "0");
    }


}
