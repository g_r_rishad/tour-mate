package com.bitm_project.tourmate.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.bitm_project.tourmate.activities.SignInActivity;

public class LogoutC {

    public static void logoutProcess(final Context context){


        AlertDialog.Builder builder= new AlertDialog.Builder(context);
        builder.setMessage("Do You Want To Exit?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                intent= new Intent(context, SignInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog= builder.create();
        alertDialog.show();

    }

    public static void forgetPassword(final Context context){


        AlertDialog.Builder builder= new AlertDialog.Builder(context);
        builder.setMessage("Please Contact With us \n tourmate@gmail.com");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        AlertDialog alertDialog= builder.create();
        alertDialog.show();

    }



}
