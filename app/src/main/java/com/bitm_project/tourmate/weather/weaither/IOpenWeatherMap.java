package com.bitm_project.tourmate.weather.weaither;


import com.bitm_project.tourmate.weather.CurrentWeather.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IOpenWeatherMap {

    @GET
    Call<WeatherResponse> getWeatherData1(@Url String url);




    @GET
    Call<WeatherResult> getWeatherData(@Url String url);
}