package com.bitm_project.tourmate.weather.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.weather.weaither.WeatherResult;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WeitherAdapter extends RecyclerView.Adapter<WeitherAdapter.ViewGroup> {

    Context context;
    WeatherResult weatherResult;
    private ProgressDialog loadinbar;


    public WeitherAdapter() {
    }

    public WeitherAdapter(WeatherResult weatherResult) {
        //this.context = context;
        this.weatherResult = weatherResult;
    }


    @NonNull
    @Override
    public ViewGroup onCreateViewHolder(@NonNull android.view.ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weither_items, parent, false);

        return new ViewGroup(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewGroup holder, int position) {

        Picasso.get().load(new StringBuilder("https://openweathermap.org/img/w/")
                .append(weatherResult.getList().get(position).getWeather().get(0).getIcon())
                .append(".png").toString()).into(holder.weitherIcon);

        SimpleDateFormat dateandTimeSDF = new SimpleDateFormat("dd MMMM yyyy");

        Date date = new Date();
        date.setDate((weatherResult.getList().get(position).getDt()));
        holder.weitherDate.setText("Date   : " + weatherResult.getList().get(position).getDt_txt());



        holder.weitherDescription.setText("Status : " + weatherResult.getList().get(position).getWeather().get(0).getDescription());
        holder.weitherTemp.setText(("Temp  : " + weatherResult.getList().get(position).getMain().getTemp() + " °C"));
        holder.weitherWind.setText("Wind : " + weatherResult.getList().get(position).getWind().getSpeed() + " km/h");

    }

    @Override
    public int getItemCount() {
         return weatherResult.getList().size();
    }

    public class ViewGroup extends RecyclerView.ViewHolder {

        private ImageView weitherIcon;
        private TextView weitherDate, weitherTemp, weitherWind, weitherHumidity, weitherDescription, weatherLoactionTv;

        public ViewGroup(@NonNull View itemView) {
            super(itemView);

            weitherIcon = itemView.findViewById(R.id.weatherItemIvId);
            weitherDate = itemView.findViewById(R.id.dateWeitherItemTvId);
            weitherTemp = itemView.findViewById(R.id.tempWeitherItemTvId);
            weitherWind = itemView.findViewById(R.id.windWeitherItemTvId);
            weitherDescription = itemView.findViewById(R.id.weitherDiscriptionTvId);
        }
    }
}
