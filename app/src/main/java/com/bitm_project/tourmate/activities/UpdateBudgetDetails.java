package com.bitm_project.tourmate.activities;

import android.app.AlertDialog;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bitm_project.tourmate.util.SharedPreferenceHandlerC;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UpdateBudgetDetails {

    private AlertDialog.Builder dialogBuilder;
    private AlertDialog  dialog;
    private EditText expenseOrBudgetNameET,expenseOrBudgetPriceET;
    private Button expenseOrBudgetCancelBtn, expenseOrBudgetSaveBtn;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private String budgetOrExpenseName,budgetOrExpensePrice,expenseName,tourName;
    private int tourCount,budgetCount=0,expenseCount=0;
    private String userId;
    private SharedPreferenceHandlerC handlerC;
    private TextView titleTV,balanceTV,expenseTV,totalBudgetTV;
    Double totalBudget=0.0,totalExpense=0.0;
    Double inputBudget=0.0,inputExpense=0.0;
    double expensePrice;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private LinearLayout seeDetailsAndBudgetLayout;

    private int exCount;


    private AppCompatActivity activity;
}
