package com.bitm_project.tourmate.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.bitm_project.tourmate.Fragment.MemoriesFragment;
import com.bitm_project.tourmate.Fragment.WalletFragment;
import com.bitm_project.tourmate.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class TourDetailsActivity extends AppCompatActivity {


    private BottomNavigationView navigationView;
    public static String tourName;
    public static int tourCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_details);


        FragmentManager fragmentManager= getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        transaction.replace(R.id.frameLayoutId,new WalletFragment());
        transaction.commit();






        tourName= getIntent().getStringExtra("tourName");
        tourCount=getIntent().getIntExtra("tourCount",0);

       // replaceFragment(new WalletFragment());

        System.out.println(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"+tourName+"/////////"+tourCount);

        init();
    }

    private void init() {

        navigationView= findViewById(R.id.bottomNavigationMenuId);
        navigationView.setBackgroundColor(Color.parseColor("#D0D3D4"));
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()){

                    case R.id.walletMenuId:
                        replaceFragment(new WalletFragment());
                     //   Toast.makeText(TourDetailsActivity.this, "Wallet", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.memoriesMenuId:
                        replaceFragment(new MemoriesFragment());
                      //  Toast.makeText(TourDetailsActivity.this, "Memory", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });

    }


//    private  void replaceFragment(Fragment fragment) {
//        String backStateName = fragment.getClass().getName();
//        String fragmentTag = backStateName;
//
//        FragmentManager manager = fm;
//        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
//
//        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
//            FragmentTransaction ft = manager.beginTransaction();
//            ft.replace(R.id.frameLayoutId, fragment, fragmentTag);
//            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//            if (!fragmentTag.equals((new WalletFragment()).getClass().getName())) {
//                ft.addToBackStack(backStateName);
//            }
//            ft.commit();
//        }
//    }

    private void replaceFragment(Fragment fragment){
        //String namr=fragment.get

        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutId,fragment).addToBackStack(null).commit();

//        FragmentManager fragmentManager= getSupportFragmentManager();
//        FragmentTransaction transaction= fragmentManager.beginTransaction();
//        transaction.replace(R.id.frameLayoutId,fragment);
//        transaction.addToBackStack(null).commit();

    }
}
