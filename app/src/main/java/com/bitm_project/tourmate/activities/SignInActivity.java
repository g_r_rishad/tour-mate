package com.bitm_project.tourmate.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bitm_project.tourmate.MainActivity;
import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.util.LogoutC;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {


    private Button logInBtn;
    private TextView signUpTV;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    private EditText emailET,passwordET;
    private String email,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        initView();
    }

    private void initView() {


        emailET= findViewById(R.id.signInActivityEmailId);
        passwordET= findViewById(R.id.signInActivityPasswordId);


        logInBtn= (Button) findViewById(R.id.loginButtonId);
        logInBtn.setOnClickListener(this);

        signUpTV=(TextView) findViewById(R.id.signUpTextId);
        signUpTV.setOnClickListener(this);

        //
        firebaseAuth= FirebaseAuth.getInstance();
        progressDialog= new ProgressDialog(this);
    }

    private boolean checkData(){

        email= emailET.getText().toString();
        password= passwordET.getText().toString();

        if (email.matches("")){
            Toast.makeText(this, "Enter Your Email", Toast.LENGTH_SHORT).show();
            return false;
        }else if (password.matches("")){
            Toast.makeText(this, "Enter Your Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()){

            case R.id.loginButtonId:

                signIn();

                break;

            case R.id.signUpTextId:
                intent= new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void signIn() {


        if (checkData()) {

            progressDialog.setMessage("Please Wait..........");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();


            firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
progressDialog.dismiss();

                    if (task.isSuccessful()){
                      Intent  intent= new Intent(SignInActivity.this, MainActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(SignInActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(SignInActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    public void forgetPassword(View view) {

        LogoutC.forgetPassword(SignInActivity.this);
    }
}
