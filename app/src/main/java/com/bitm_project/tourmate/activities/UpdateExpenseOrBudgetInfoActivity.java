package com.bitm_project.tourmate.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bitm_project.tourmate.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UpdateExpenseOrBudgetInfoActivity extends AppCompatActivity {

    private AlertDialog.Builder dialogBuilder;
    private AlertDialog  dialog;
    private TextView titleTV;
    private EditText expenseOrBudgetNameET,expenseOrBudgetPriceET;
    private Button expenseOrBudgetCancelBtn, expenseOrBudgetSaveBtn;

    //
    private FirebaseAuth firebaseAuth=FirebaseAuth.getInstance();
    private DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference();
    String userId;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_expense_or_budget_info);
    }


    public void updateExpenseDetails(String expenseName, double expensePrice) {

        createAddExpensePopUp(expenseName,expensePrice);
    }

    private void createAddExpensePopUp(String expenseName, double expensePrice){


        dialogBuilder= new AlertDialog.Builder(this);

        LayoutInflater inflater= LayoutInflater.from(this);
        View view= inflater.inflate(R.layout.add_budget_layout,null);

        titleTV= view.findViewById(R.id.addExpenseOrBudgetTitleId);
        titleTV.setText("Edit Expense");

        //
        expenseOrBudgetNameET= view.findViewById(R.id.budgetCategoryNameId);
        expenseOrBudgetPriceET= view.findViewById(R.id.budgetCategoryPriceId);

        //
        expenseOrBudgetNameET.setText(expenseName);
        expenseOrBudgetPriceET.setText(String.valueOf(expensePrice));

        expenseOrBudgetSaveBtn=view.findViewById(R.id.saveBudgetOrExpenseButtonId);
        expenseOrBudgetSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // saveExpensenfo();
            }
        });

        expenseOrBudgetCancelBtn= view.findViewById(R.id.cancelBudgetOrExpenseButtonId);
        expenseOrBudgetCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialogBuilder.setView(view);
        dialog= dialogBuilder.create();
        dialog.show();

    }
}
