package com.bitm_project.tourmate.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.model.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText firstNameET,lastNameET,phoneNumberET,emailET,passwordET,confirmPasswordET;
    private Button signUpBtn,imageUp;
    private String firstName,lastName,phoneNumber,email,password,confirmPassword,emailPattern;
    private ImageView profilePicImg,showImage;
    private Bitmap bitmapImage= null;
    private Uri selectedImage;
    private TextView signInTV;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog alertDialog;
    private FloatingActionButton profilePicFloatingButton;
    private StorageReference storageReference;
    private byte[] aa;
    private String picSource,downloadLink,userId;
    private Uri downloadUri;
    private UserInfoModel userInfoModel;
    private ProgressDialog progressDialog;

    //
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

          emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        initView();
    }

    private void initView() {

        // Edit Text
        firstNameET= findViewById(R.id.firstNameId);
        lastNameET=findViewById(R.id.lastNameId);
        phoneNumberET=findViewById(R.id.phoneNumberId);
        emailET= findViewById(R.id.emailId);
        passwordET=findViewById(R.id.passwordId);
        confirmPasswordET= findViewById(R.id.confirmPasswordId);
         profilePicImg= findViewById(R.id.profilePicImageId);

        //Button
        signUpBtn= findViewById(R.id.signUpButtonId);
        signUpBtn.setOnClickListener(this);
        profilePicFloatingButton= findViewById(R.id.selectProfilePicImageFabButtonId);
        profilePicFloatingButton.setOnClickListener(this);

        // TextView
        signInTV= findViewById(R.id.signInTextId);
        signInTV.setOnClickListener(this);

        //
        storageReference= FirebaseStorage.getInstance().getReference();
        //

        //showImage= findViewById(R.id.showImageId);
//        imageUp= findViewById(R.id.imageUpButtonId);
//        imageUp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                uploadProfilePic();
//            }
//        });


        //
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();

        //
        progressDialog= new ProgressDialog(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.signUpButtonId:
                uploadProfilePic();
                break;
            case R.id.selectProfilePicImageFabButtonId:
                choosePicDialog();
                break;
            case R.id.signInTextId:
                Intent intent= new Intent(SignUpActivity.this,SignInActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void choosePicDialog() {

        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        String[] items= {"From Camera","From Gallery"};
        builder.setTitle("Choose an Action");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which){
                    case 0:
                        //picSource="camera";
                        openCamera();
                        break;
                    case 1:
                       // picSource="gallery";
                        openGallery();
                        break;
                }
            }
        });

        Dialog dialog= builder.create();
        dialog.show();
    }

    private void openGallery() {

        Intent intent= new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/**");
        startActivityForResult(intent,99);
    }

    private void openCamera() {
        Intent intent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,88);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       // StorageReference profilePicRef= storageReference.child("profilePic"+ UUID.randomUUID());


        if (resultCode==RESULT_OK){
            if (requestCode==88){

                if (data != null){

                    picSource="camera";

                    Bundle bundle= data.getExtras();
                    bitmapImage=(Bitmap) bundle.get("data");
                   // selectedImage=data.getData();
                    profilePicImg.setImageBitmap(bitmapImage);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    aa = baos.toByteArray();

                }


            }
            else if (requestCode==99){


                if (data != null){
                    picSource="gallery";
                    selectedImage= data.getData();

                    try {
                        Bitmap bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                        profilePicImg.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // useImageUri(selectedImage);

                }else {
                    Toast.makeText(this, "Select Image", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

        }
    }


    private boolean getData() {

        firstName= firstNameET.getText().toString();
        lastName= lastNameET.getText().toString();
        phoneNumber= phoneNumberET.getText().toString();
        email= emailET.getText().toString();
        password=passwordET.getText().toString();
        confirmPassword=confirmPasswordET.getText().toString();


        if (selectedImage == null && bitmapImage == null){
            Toast.makeText(this, "Select Image", Toast.LENGTH_SHORT).show();
            return false;
        }else if (firstName.matches("")){
            Toast.makeText(this, "Enter First Name.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (lastName.matches("")){
            Toast.makeText(this, "Enter Last Name.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (phoneNumber.matches("")){
            Toast.makeText(this, "Enter Phone Number.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (email.matches("")){
            Toast.makeText(this, "Enter Email.", Toast.LENGTH_SHORT).show();
            return false;
        }else if ( !email.matches(emailPattern)){
            Toast.makeText(SignUpActivity.this, "Enter Valid Email Address", Toast.LENGTH_SHORT).show();
            return false;
        }else if (password.matches("")){
            Toast.makeText(this, "Enter Password.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (password.length() <= 5){
            Toast.makeText(this, "Password Length less than 6.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (confirmPassword.matches("")){
            Toast.makeText(this, "Enter Confirm Password.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (confirmPassword.length() <=5){
            Toast.makeText(this, "Confirm Password length less than 6.", Toast.LENGTH_SHORT).show();
            return false;
        }

//        else {
//
//            if (password.matches(confirmPassword)){
//
//                signUp(firstName,lastName,phoneNumber,email,password,confirmPassword);
//
//            }else {
//                Toast.makeText(this, "Password not match.", Toast.LENGTH_SHORT).show();
//            }
//        }
        return true;
    }


    private void uploadProfilePic() {

        if (getData()) {

            progressDialog.setMessage("Please wait..");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            StorageReference profilePicRef = storageReference.child("profilePic" + UUID.randomUUID());

            if (picSource.matches("camera")) {


                profilePicRef.putBytes(aa).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        if (task.isSuccessful()){
                           // Toast.makeText(SignUpActivity.this, "Upload Successfully", Toast.LENGTH_SHORT).show();
                            signUp();
                        }else {
                            Toast.makeText(SignUpActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();
                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(SignUpActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                        Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();
                        while (!uri.isComplete()) ;
                        Uri url = uri.getResult();

                        downloadLink = url.toString();

                        System.out.println(".................................................................." + downloadLink);

                 //       Picasso.get().load(url).into(showImage);

                    }
                });

            } else if (picSource.matches("gallery")) {

                profilePicRef.putFile(selectedImage).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {


                        if (task.isSuccessful()) {

                            Toast.makeText(SignUpActivity.this, "Upload Successfully", Toast.LENGTH_SHORT).show();
                           signUp();
                        }else {
                            Toast.makeText(SignUpActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(SignUpActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadLink = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();

                        //Picasso.get().load(downloadLink).into(showImage);
                    }
                });
            }
        }
    }

    private void signUp(){

        firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){

                    userId= firebaseAuth.getCurrentUser().getUid();
                    DatabaseReference userInfoRef=databaseReference.child("tourmate").child("user's").child(userId);

                    userInfoModel= new UserInfoModel(firstName,lastName,phoneNumber,email,password,confirmPassword,downloadLink);

                    userInfoRef.setValue(userInfoModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            progressDialog.dismiss();
                            if (task.isSuccessful()){
                                Toast.makeText(SignUpActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                                Intent intent= new Intent(SignUpActivity.this,SignInActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(SignUpActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Toast.makeText(SignUpActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(SignUpActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
