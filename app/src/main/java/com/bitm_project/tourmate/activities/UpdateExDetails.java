package com.bitm_project.tourmate.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bitm_project.tourmate.Fragment.ExpenseDetailsFragment;
import com.bitm_project.tourmate.MainActivity;
import com.bitm_project.tourmate.R;
import com.bitm_project.tourmate.model.AddExpenseModel;
import com.bitm_project.tourmate.model.TotalExpenseModel;
import com.bitm_project.tourmate.util.SharedPreferenceHandlerC;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UpdateExDetails {


    private FloatingActionButton addBudgetFabBtn, addExpenseFabBtn;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog  dialog;
    private EditText expenseOrBudgetNameET,expenseOrBudgetPriceET;
    private Button expenseOrBudgetCancelBtn, expenseOrBudgetSaveBtn;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private String budgetOrExpenseName,budgetOrExpensePrice,expenseName,tourName;
    private int tourCount,budgetCount=0,expenseCount=0;
    private String userId;
    private SharedPreferenceHandlerC handlerC;
    private TextView titleTV,balanceTV,expenseTV,totalBudgetTV;
    Double totalBudget=0.0,totalExpense=0.0;
    Double inputBudget=0.0,inputExpense=0.0;
    double expensePrice;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private LinearLayout seeDetailsAndBudgetLayout;

    private int exCount;






    private AppCompatActivity activity;

    public UpdateExDetails(AppCompatActivity activity) {

        this.activity= activity;

        handlerC= new SharedPreferenceHandlerC(activity);

        tourCount= TourDetailsActivity.tourCount;
        tourName= TourDetailsActivity.tourName;

    }


    public void updateExpenseInfo(String expenseName,double expensePrice,int expenseCount){

        createAddBudgetPopUp(expenseName,expensePrice,expenseCount);

    }

    private void createAddBudgetPopUp(String expenseName,double expensePrice,int expenseCount){


        exCount= expenseCount;
       this.expensePrice=expensePrice;

        dialogBuilder= new AlertDialog.Builder(activity);

        LayoutInflater inflater= LayoutInflater.from(activity);
        View view= inflater.inflate(R.layout.add_budget_layout,null);

        titleTV= view.findViewById(R.id.addExpenseOrBudgetTitleId);
        titleTV.setText("Update Expense");

        //
        expenseOrBudgetNameET= view.findViewById(R.id.budgetCategoryNameId);

        expenseOrBudgetNameET.setText(expenseName);
        expenseOrBudgetPriceET= view.findViewById(R.id.budgetCategoryPriceId);
        expenseOrBudgetPriceET.setText(String.valueOf(expensePrice));

        expenseOrBudgetSaveBtn=view.findViewById(R.id.saveBudgetOrExpenseButtonId);
        expenseOrBudgetSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateExpensenfo();
            }
        });

        expenseOrBudgetCancelBtn= view.findViewById(R.id.cancelBudgetOrExpenseButtonId);
        expenseOrBudgetCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogBuilder.setView(view);
        dialog= dialogBuilder.create();
        dialog.show();

    }

    private void UpdateExpensenfo() {


        userId=firebaseAuth.getCurrentUser().getUid();

//        expenseCount=Integer.valueOf(handlerC.getExpenseCount());
//        expenseCount=expenseCount+1;
//        handlerC.expenseCount(expenseCount);


        if (checkValidation()){


            totalExpense= Double.parseDouble(handlerC.getTotalExpense());
            inputExpense= Double.parseDouble(budgetOrExpensePrice);

            double value=0;

            if (inputExpense > expensePrice){

                 value= inputExpense-expensePrice;
                totalExpense= totalExpense+value;

                //Toast.makeText(activity, "Input", Toast.LENGTH_SHORT).show();

            } else if (expensePrice > inputExpense) {

                value= expensePrice - inputExpense;
                totalExpense= totalExpense - value;

              //Toast.makeText(activity, "Parameter", Toast.LENGTH_SHORT).show();
            }


          //  totalExpense= totalExpense+inputExpense;
            totalExpenseSummaryInfo(totalExpense);

             //handlerC.setTotalExpense(totalExpense);


            DatabaseReference updateExpenseInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                    child("tour "+tourCount).child("tourAccount").child("expenseInfo").child("expense "+exCount);


            AddExpenseModel expenseModel= new AddExpenseModel(budgetOrExpenseName,Double.valueOf(budgetOrExpensePrice),exCount);

            updateExpenseInfoRef.setValue(expenseModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();

                        }
                    },1000);

                    if (task.isSuccessful()){
                        Toast.makeText(activity, "Expense Successfully Update", Toast.LENGTH_SHORT).show();
                  //  updateBudgetAndExpenseInfo();
                    }else{
                        Toast.makeText(activity, ""+task.getException(), Toast.LENGTH_SHORT).show();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(activity, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean checkValidation(){

        budgetOrExpenseName= expenseOrBudgetNameET.getText().toString();
        budgetOrExpensePrice= expenseOrBudgetPriceET.getText().toString();

        if (budgetOrExpenseName.matches("")){
            Toast.makeText(activity, "Enter Budget Name", Toast.LENGTH_SHORT).show();
            return false;
        }else if (budgetOrExpensePrice.matches("")){
            Toast.makeText(activity, "Enter Budget Price", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    private void totalExpenseSummaryInfo(double expenseBudget){

        userId= firebaseAuth.getCurrentUser().getUid();

        DatabaseReference saveTotalBudgetInfoRef= databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour "+tourCount).child("tourAccount").child("expenseSummaryInfo");

        TotalExpenseModel expenseModel= new TotalExpenseModel("Total Expense",expenseBudget);

        saveTotalBudgetInfoRef.setValue(expenseModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                    refresh();

                   // Toast.makeText(activity, "Total Expense Saved !!", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(activity, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void deleteExpenseInfo(String expenseName, final double expensePrice, int expenseCount) {


        userId = firebaseAuth.getCurrentUser().getUid();

        final DatabaseReference deleteExpenseInfoRef = databaseReference.child("tourmate").child("tourHistory").child(userId).
                child("tour " + tourCount).child("tourAccount").child("expenseInfo").child("expense " + expenseCount);

        deleteExpenseInfoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot deleteData : dataSnapshot.getChildren()) {
                    deleteExpenseInfoRef.removeValue();

                    Toast.makeText(activity, "Expense Deleted", Toast.LENGTH_SHORT).show();

                    double deleteExpensePrice=expensePrice;
                    totalExpense= Double.parseDouble(handlerC.getTotalExpense());
                   totalExpense= totalExpense - expensePrice;
                    totalExpenseSummaryInfo(totalExpense);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(activity, ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void refresh(){

        FragmentManager fragmentManager= activity.getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        transaction.replace(R.id.budgetAndSalaryDetailsFrameLayoutId,new ExpenseDetailsFragment());
        transaction.commit();

    }


}
